import numpy as np

n = 10000
dim = 9
A = [np.random.uniform(-1, 1, dim) for i in range(n)]
mA = reduce(lambda x, y: x+y, A) / len(A)
dA = [np.linalg.norm(mA - v) for v in A]
print np.sum(dA) / len(A)
