import re
import json
import glob
import gensim, logging
import numpy as np

TEST_PROG = False
game_history = []
players_set = set()
players_dict = {}

f_list = glob.glob('./opendota/full_matches/*.html')
if TEST_PROG: f_list = f_list[:1000]
for fname in f_list:
    # m = re.search('matches_(.*?)[.]', fname)
    # mid = m.group(1)
    with open(fname, 'rb') as f:
        j = json.load(f)
    team_A, team_B = [], []
    game_res = 0 if j["players"][0]["win"] == 1 else 0
    for p in range(10):
        pid = j["players"][p]["account_id"]
        players_set.add(pid)
        if p < 5: team_A.append(pid)
        if p >= 5: team_B.append(pid)
    game_history.append([[team_A, team_B], game_res])

players = sorted(list(players_set))
for i, v in enumerate(players):
    players_dict[v] = str(i)

def replace_pid(team):
    for i in range(len(team)):
        team[i] = players_dict[team[i]]

for game in game_history:
    replace_pid(game[0][0])
    replace_pid(game[0][1])

n_games = len(game_history)
n_players = len(players)
n_train_games = max(1, int(n_games * 0.8))
n_test_games = n_games - n_train_games
train_game_history = game_history[:n_train_games]
test_game_history = game_history[n_train_games:]

def history_to_sentence(game_history):
    res = [map(str, list(x[0][x[1]])) for x in game_history]
    for x in game_history:
        for p in x[0][1 - x[1]]:
            res += [[str(p)]]
    return res

sentences = history_to_sentence(train_game_history)
print n_players
model = gensim.models.Word2Vec(sentences, size=int(n_players**0.5), min_count=1)
# print model.wv['1'], model.wv['2']
# print model.wv.similarity('1', '2')

def sum_sim(team):
    tv = []
    for p in team:
        tv.append(model.wv[str(p)])
    mv = reduce(lambda x, y: x+y, tv)
    mv /= 5.0
    dv = [np.linalg.norm(v - mv) for v in tv]
    res = sum(dv)
    return res

def predict_by_sim(team_A, team_B):
    sa, sb = sum_sim(team_A), sum_sim(team_B)
    return 0 if sa < sb else sb < sa

pred = [predict_by_sim(g[0][0], g[0][1]) for g in train_game_history]
ground = [g[1] for g in train_game_history]
pred, ground = np.array(pred), np.array(ground)
print np.sum(pred == ground) / float(len(pred))
