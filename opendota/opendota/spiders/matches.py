import scrapy
import json

class MatchesSpider(scrapy.Spider):
    name = "matches"

    custom_settings = {
        'DOWNLOAD_DELAY': 0.30,
    }

    def start_requests(self):
	match_base_url = "https://api.opendota.com/api/matches/"
	start_index = 28000
	end_index = int(1e9)
        for index in xrange(start_index, end_index):
            url = match_base_url + "%d"%index
            print url
            yield scrapy.Request(url=url, callback=self.parse)
        # f = file("match_ids")
        # ids = map(lambda x: x[:-1], list(f))
        # print ids
        # for index in ids:
        #     url = match_base_url + "%s"%index
        #     print url
        #     yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url.split("/")[-1]
        filename = 'matches_%d.html' % int(page)
        # jsonresponse = json.loads(response.body_as_unicode())
        # print jsonresponse
        with open("matches/%s"%filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)

