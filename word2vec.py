import numpy as np
from itertools import combinations
import gensim, logging
from sklearn import metrics
import random
import beeprint

class IdealSimulationGame(object):
    name = 'IdealSimulation'
    def __init__(self, n_types=5, sz_team=5, n_players=1000, 
            n_train_games=100000, n_test_games=1000,
            NormalDistJoining=True, beta=3):
        self.n_types = n_types
        self.sz_team = sz_team
        self.n_players = n_players
        self.n_train_games = n_train_games
        self.n_test_games = n_test_games
        self.NormalDistJoining = NormalDistJoining
        self.beta = beta

        self.n_cc = 0
        self.sum_cc = 0.0

        self.gen_players()
        self.players_dist = self.normal_p()

    def normal_p(self):
        s = np.random.normal(0.5, 0.3, self.n_players)
        s = np.clip(s, 0, 1)
        s /= np.sum(s)
        return s

    def gen_players(self):
        self.players = np.random.randint(1, high=self.n_types, size=self.n_players)

    def gen_team(self):
        res = np.zeros(self.n_players)
        if self.NormalDistJoining:
            index = np.random.choice(self.n_players, self.sz_team, p=self.players_dist)
        else:
            index = np.random.choice(self.n_players, self.sz_team)
        return index # for i in index: res[i] = 1

    def team_valid(self, A, B):
        if A is None or B is None: return False
        for x in B: 
            if x in A: return False
        return True

    def team_variance(self, team):
        mv = np.average(team, axis=0)
        dv = [np.linalg.norm(team[i] - mv) for i in range(len(team))]
        res = sum(dv) / float(len(team))
        return res

    def base_team_performance(self):
        return np.sum(np.random.normal(10, self.beta, self.sz_team))

    def team_to_points(self, team):
        players_one_hot = np.zeros((len(team), self.n_types))
        for i, x in enumerate(team): players_one_hot[i, self.players[x]] = 1
        variance = self.team_variance(players_one_hot)
        if variance == 0: variance = 0.01
        cc = (1./variance)
        self.n_cc += 1
        self.sum_cc += cc
        pts = cc * self.base_team_performance()
        return pts

    def gen_match(self):
        A, B = None, None
        while not self.team_valid(A, B):
            A, B = self.gen_team(), self.gen_team()
        pt_A, pt_B = self.team_to_points(A), self.team_to_points(B)
        if pt_A == pt_B: res = random.randint(0, 1)
        else: res = 0 if pt_A > pt_B else 1
        return [[A, B], res]

class VectorAttriSimulationGame(IdealSimulationGame):
    name = 'VectorAttriSimulationGame'
    def gen_players(self):
        self.players = np.random.uniform(
                0, 5.0,
                size=(self.n_players, self.n_types))

    def team_to_points(self, team):
        team_vectors = self.players[team, :]
        variance = self.team_variance(team_vectors)
        if variance == 0: variance = 0.01
        cc = (1./variance)
        self.n_cc += 1
        self.sum_cc += cc
        pts = cc * self.base_team_performance()
        return pts

class TypesDistanceSimulationGame(IdealSimulationGame):
    name = 'TypesDistanceSimulationGame'
    def team_variance(self, team):
        mv = np.average(team)
        dv = [np.linalg.norm(team[i] - mv) for i in range(len(team))]
        res = sum(dv) / float(len(team))
        return res

    def team_to_points(self, team):
        team_type = np.zeros(self.n_types)
        for i, x in enumerate(team): team_type[i] = self.players[x]
        variance = self.team_variance(team_type)
        if variance == 0: variance = 0.01
        pts = (1./variance) * self.base_team_performance()
        return pts

class MoreTypesSimulationGame(IdealSimulationGame):
    name = 'MoreTypesSimulationGame'
    def team_to_points(self, team):
        team_types = np.zeros(self.n_types)
        for x in team: team_types[self.players[x]] = 1
        cc = np.sum(team_types)
        self.n_cc += 1
        self.sum_cc += cc
        pts = cc * self.base_team_performance()
        return pts

class SameTypesSimulationGame(IdealSimulationGame):
    name = 'SameTypesSimulationGame'
    def team_to_points(self, team):
        team_types = np.zeros(self.n_types)
        for x in team: team_types[self.players[x]] += 1
        cc = np.max(team_types)
        self.n_cc += 1
        self.sum_cc += cc
        pts = cc * self.base_team_performance()
        return pts

class TypesVarianceSimulationGame(IdealSimulationGame):
    name = 'TypesVarianceSimulationGame'
    def team_to_points(self, team):
        team_types = np.zeros(self.n_types)
        for x in team: team_types[self.players[x]] = 1
        cc = self.team_variance(team_types)
        self.n_cc += 1
        self.sum_cc += cc
        pts = cc * self.base_team_performance()
        return pts

    def gen_match(self):
	A, B = None, None
	while not self.team_valid(A, B):
	    A, B = self.gen_team(), self.gen_team()
	pt_A, pt_B = self.team_variance(A), self.team_variance(B)
	pt_A = pt_A * (1. / np.sum(np.random.normal(10, 3, self.n_players)))
	pt_B = pt_B * (1. / np.sum(np.random.normal(10, 3, self.n_players)))
	if pt_A == pt_B: res = random.randint(0, 1)
	else: res = 0 if pt_A < pt_B else 1
	return [[A, B], res]


class IdealPredictor(object):
    name = 'IdealPredictor'
    def __init__(
            self, n_types=5, 
            sz_team=5, n_players=1000, 
            n_train_games=100000, n_test_games=1000,
            simulator=None, show_performace_now=False,
            NormalDistJoining=True, name='',
            beta=3):
        self.n_types = n_types
        self.sz_team = sz_team
        self.n_players = n_players
        self.n_train_games = n_train_games
        self.n_test_games = n_test_games
        self.name = name + self.name

        self.n_trained_threshold = 5
        self.simulator = simulator(
                n_types=n_types, 
                sz_team=sz_team, 
                n_players=n_players, 
                n_train_games=n_train_games, 
                n_test_games=n_test_games,
                NormalDistJoining=NormalDistJoining,
                beta=beta)
        self.name += simulator.name
        self.model = None

        if show_performace_now: self.show_performace()

    def get_name(self):
        return self.name

    def get_avg_cc(self):
        return self.simulator.sum_cc / self.simulator.n_cc

    def history_to_sentence(self, game_history):
        res = [map(str, list(x[0][x[1]])) for x in game_history]
        return res

    def train(self):
        train_game_history = [self.simulator.gen_match() 
                for x in range(self.n_train_games)]
        sentences = self.history_to_sentence(train_game_history)

        self.model = gensim.models.Word2Vec(sentences, size=int(self.n_players**0.5), 
                min_count=1, iter=5, sg=1, workers=4)

    def test(self):
        test_game_history = [self.simulator.gen_match() 
                for x in range(self.n_test_games)]
        self.sz_vocab = len(self.model.wv.vocab)
        self.vocab = dict(self.model.wv.vocab)
        self.list_vocab = list(self.model.wv.vocab)
        self.avg_var = self.avg_variance(self.model.wv)
        pred = [self.predict_by_sim(g[0][0], g[0][1]) for g in test_game_history]
        ground = [g[1] for g in test_game_history]
        pred, ground = np.array(pred), np.array(ground)
        fpr, tpr, thresholds = metrics.roc_curve(ground, pred)
        acc = np.sum(pred == ground) / float(len(pred))
        # print 'acc:', acc
        # print 'auc:', metrics.auc(fpr, tpr)
        return acc

    def show_performace(self):
        res = {}
        self.train()
        res['acc'] = self.test()
        return res

    def sum_sim(self, team):
        tv = []
        for p in team:
            tv.append(self.model.wv[str(p)])
        mv = reduce(lambda x, y: x+y, tv)
        mv /= float(len(team))
        dv = [np.linalg.norm(v - mv) for v in tv]
        res = sum(dv) / float(len(team))
        return res

    def avg_variance(self, model):
        sz_sample_voacb = int(self.n_players)
        tv = []
        sz_model = 0
        # sum_var = 0.0
        var = np.zeros(sz_sample_voacb)
        for vi in xrange(sz_sample_voacb):
            index = np.random.choice(self.sz_vocab, self.sz_team)
            team = map(lambda i: self.list_vocab[i], index)
            s = self.sum_sim(team)
            var[vi] = s
            # sum_var += s
        var = np.sort(var)
        # res = sum_var / float(sz_sample_voacb)
        res = np.mean(var[int(len(var)*0.2):int(len(var)*0.8)])
        return res

    def sparse_sim(self, team):
        tv = []
        n_notsure = 0.0
        for p in team:
            if str(p) in self.vocab: tv.append(self.model.wv[str(p)])
            else: n_notsure += 1
        mv = reduce(lambda x, y: x+y, tv)
        mv /= float(len(team))
        dv = [np.linalg.norm(v - mv) for v in tv]
        var = (sum(dv) + n_notsure * self.avg_var) / float(len(team))
        res = 1./var
        return res

    def predictable(self, team_A, team_B):
        n_trained = 0
        for p in team_A + team_B: 
            if str(p) in vocab: n_trained += 1
        return True if n_trained >= self.n_trained_threshold else False

    def predict_by_sim(self, team_A, team_B):
        sa, sb = self.sparse_sim(team_A), self.sparse_sim(team_B)
        return 0 if sa > sb else 1

class SimilarityPredictor(IdealPredictor):
    # The perforamce of the SimilarityPredictor is similar to the IdealPredictor
    name = 'SimilarityPredictor'
    def sparse_sim(self, team):
        tv = []
        n_notsure = 0.0
        for p in team:
            if str(p) in self.vocab: tv.append(str(p))
            else: n_notsure += 1
        n_sim = 0
        sum_sim = 0.0
        for v1, v2 in combinations(tv, 2):
            sum_sim += self.model.similarity(v1, v2)
            n_sim += 1
        res = sum_sim / n_sim
        return res

def standard_experiment(predictor, simulation, param, n_repeat=1):
    perf = np.zeros(n_repeat)
    exper_predictor = predictor(
            n_types=param.n_types, 
            sz_team=param.sz_team, 
            n_players=param.n_players, 
            n_train_games=param.n_train_games, 
            n_test_games=param.n_test_games,
            beta=param.beta,
            simulator=simulation,
            show_performace_now=False,
            NormalDistJoining=False)
    exper_name = exper_predictor.get_name()
    print 'The Performance of', exper_name
    for i in xrange(n_repeat):
        perf[i] = exper_predictor.show_performace()['acc']
    print 'Average of Accuracy:', np.average(perf)
    print 'Std of Accuracy:', np.std(perf)

def beta_experiment(predictor, simulation, param, n_repeat=1):
    beta = np.linspace(0, 5, 10)
    perf = np.zeros(len(beta))
    avg_cc = np.zeros(len(beta))

    exper_name = 'Beta'+predictor(simulator=simulation).get_name()
    print 'The Performance of', exper_name

    for i in range(len(beta)):
        exper_predictor = predictor(
                n_types=param.n_types, 
                sz_team=param.sz_team, 
                n_players=param.n_players, 
                n_train_games=param.n_train_games, 
                n_test_games=param.n_test_games,
                beta=beta[i],
                simulator=simulation,
                show_performace_now=False,
                NormalDistJoining=False)
        repeat_perf = np.zeros(n_repeat)
        for j in xrange(n_repeat):
            res = exper_predictor.show_performace()
            repeat_perf[j] = res['acc']
        perf[i] = np.average(repeat_perf)
        avg_cc[i] = exper_predictor.get_avg_cc()
    print 'beta:', ' '.join(map(lambda x: '%2.3f'%x, beta))
    print 'a_cc:', ' '.join(map(lambda x: '%2.3f'%x, avg_cc))
    print 'perf:', ' '.join(map(lambda x: '%2.3f'%x, perf))

class StandParams(object):
    def __init__(self, 
            n_types=2,
            sz_team=5, 
            n_players=1000, 
            n_train_games=100000, 
            n_test_games=1000, 
            beta=3):
        self.n_types=n_types
        self.sz_team=sz_team
        self.n_players=n_players
        self.n_train_games=n_train_games
        self.n_test_games=n_test_games
        self.beta=beta

print beeprint.pp(StandParams())

standard_experiment(IdealPredictor, VectorAttriSimulationGame, StandParams(), n_repeat=1)
standard_experiment(IdealPredictor, IdealSimulationGame, StandParams(), n_repeat=5)
standard_experiment(IdealPredictor, SameTypesSimulationGame, StandParams(), n_repeat=5)
standard_experiment(IdealPredictor, MoreTypesSimulationGame, StandParams(), n_repeat=5)
standard_experiment(IdealPredictor, TypesDistanceSimulationGame, StandParams(), n_repeat=5)

beta_experiment(IdealPredictor, SameTypesSimulationGame, StandParams(), n_repeat=3)
