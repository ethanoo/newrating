import numpy as np
import random

n_players = 100
n_types = 5
sz_team = 5
n_train_games = 100000
n_test_games = 100
players = np.random.randint(1, high=n_types, size=n_players)

def gen_team():
    res = np.zeros(n_players)
    index = np.random.choice(n_players, sz_team)
    return index
    # for i in index: res[i] = 1
    # return res

def team_valid(A, B):
    if A is None or B is None: return False
    for x in B: 
        if x in A: return False
    return True

def gen_match():
    A, B = None, None
    while not team_valid(A, B):
        A, B = gen_team(), gen_team()
    A_types = np.zeros(n_types)
    B_types = np.zeros(n_types)
    for x in A: A_types[players[x]] = 1
    for x in B: B_types[players[x]] = 1
    pt_A, pt_B = np.sum(A_types), np.sum(B_types)
    if pt_A == pt_B: res = random.randint(0, 1)
    else: res = 0 if pt_A > pt_B else 1
    return [[A, B], res]

def to_one_hot(d):
    res = np.zeros(n_players)
    for x in d: res[x] = 1
    return res

def history_to_embedding_xy(game_history):
    input_x = [x[0][x[1]] for x in game_history] + [x[0][1 - x[1]] for x in game_history]
    input_y = [1 for x in game_history] + [0 for x in game_history]
    # input_x = [x[0][x[1]] for x in game_history]
    # input_y = [1 for x in game_history]
    train_input_x, train_input_y = np.array(input_x), np.array(input_y)
    return train_input_x, train_input_y

def history_to_battle_xy(game_history):
    input_x_A = [x[0][0] for x in game_history]
    input_x_B = [x[0][1] for x in game_history]
    input_y = [x[1] for x in game_history]
    x, y = [np.array(input_x_A), np.array(input_x_B)], np.array(input_y)
    return x, y

train_game_history = [gen_match() for x in range(n_train_games)]
test_game_history = [gen_match() for x in range(n_test_games)]

train_input_x, train_input_y = history_to_embedding_xy(train_game_history)
test_input_x, test_input_y = history_to_embedding_xy(test_game_history)

train_battle_input_x, train_battle_input_y = history_to_battle_xy(train_game_history)
test_battle_input_x, test_battle_input_y = history_to_battle_xy(test_game_history)

import keras
from keras import optimizers
from keras.models import Sequential, Model
from keras.layers import Dense, Input
from keras.layers import Flatten
from keras.layers.embeddings import Embedding

model = Sequential()
embedding_layer = Embedding(n_players, n_types, input_length=sz_team)
model.add(embedding_layer)
model.add(Flatten())
model.add(Dense(5, activation='sigmoid'))
model.add(Dense(1, activation='sigmoid'))
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])
print model.summary()

team_A_input = Input(shape=(sz_team,), dtype='int32',  name='team_A_input')
team_B_input = Input(shape=(sz_team,), dtype='int32',  name='team_B_input')
team_A_embed = embedding_layer(team_A_input)
team_B_embed = embedding_layer(team_B_input)
battle_layer = keras.layers.concatenate([team_A_embed, team_B_embed])
battle_flatten = Flatten()(battle_layer)
battle_dense = Dense(50, activation='sigmoid')(battle_flatten)
battle_output = Dense(1, activation='sigmoid', name='battle_output')(battle_flatten)
battle_model = Model(inputs=[team_A_input, team_B_input], outputs=[battle_output])
battle_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])
print battle_model.summary()

model.fit(train_input_x, train_input_y, epochs=20, validation_split=0.2, shuffle=True)
_, embed_accuracy = model.evaluate(test_input_x, test_input_y, verbose=0)
print('Embedding Accuracy: %f' % (embed_accuracy*100))

battle_model.fit(train_battle_input_x, train_battle_input_y, epochs=20, validation_split=0.2)
_, battle_accuracy = battle_model.evaluate(test_battle_input_x, test_battle_input_y, verbose=0)
print('Battle Accuracy: %f' % (battle_accuracy*100))
